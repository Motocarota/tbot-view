import React from 'react';

class App extends React.Component {
  constructor () {
    super()
    this.state = {}
  }
  componentWillMount () {
    window.fetch('/api/config/1').then(
      res => res.json()
    ).then(
      json => this.setState({
        config: json
      })
    )
  }
  render() {
    const {
      config
    } = this.state
    return (
      <pre>
        config: {JSON.stringify(config, 0, 2)}
      </pre>
    );
  }
}

export default App;
