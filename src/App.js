import React, { Component } from 'react';
import Config from './Config'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Config />
      </div>
    );
  }
}

export default App;
