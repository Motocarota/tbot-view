#!/bin/bash
echo updating tbot public folder...
cd ~/var/tbot-view/
npm run build
rm -rf ../tbot/public
cp ./build ../tbot/public

echo updating repository...
cd ~/var/tbot
git status
git commit -am 'updates viewer UI'
git push

echo done! gg
