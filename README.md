# TBOT view

Connects to Bitfinex trading bot instances to visually display important informations about session execution:

- current configuration
- session information
- log files
- profit and losses

# Getting started

```
git clone ...
cd tbot-view
npm install
npm start
```

# TBOT API Endpoints

```
- GET /api/config
	return {String_List}: config id list

- GET /api/config/<config_id>
	return Object
	{
	  // meta fields
	  ID: Integer
	  NAME: String
	  DESC: String
	  DESTINATION: String

	  // data fields
	  FEES: Float
	  DELTA_BUY: Float
	  DELTA_SELL: Float
	  ORDER_SIZE: Float
	  ORDER_INTERVAL: Float
	  ORDER_LIFESPAN: Float
	  ORDER_POOL: Integer
	  PAIR: String
	  SUB_CANDLES: Bool
	  SUB_BOOK: Bool
	  CHECK_BS: Function () => Bool
	}

- GET /api/session
	return {String_List}: session id list

- GET /api/session/<session_id>
	return {Object}: session info Object
	{
		id: String
		status: String
		wallet: Object
		gain: Float
		orders: Array<Object>
		lastActivity: Timestamp
	}

- GET /api/log
	return {String_List}: list of available logs session_id

- GET /api/log/<session_id>
	return {Array<String>}: logs of the session
```